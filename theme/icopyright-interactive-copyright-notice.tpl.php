<?php
/**
 * @file
 * Theme file that renders the interactive copyright notice (a copyright notice with a hyperlink to iCopyright)
 */

/**
 * Displays the interactive copyright notice, normally at the bottom of your article. It includes
 * both the copyright notice and the name of your publication. When clicked it takes the reader to
 * the main menu of services for your article.
 *
 * You can theme the copyright notice however you like. The hyperlink for the story is
 *
 * http://license.icopyright.net/3.[$pubid]?icx_id=[$node->nid]
 */
?>
<!-- iCopyright Interactive Copyright Notice -->
<a class="icopyright-interactive-notice"
  onmouseover="javascript: this.getElementsByTagName('img')[0].src='<?php print $server ?>/images/icopy-g.png';"
  onmouseout="javascript: this.getElementsByTagName('img')[0].src='<?php print $server ?>/images/icopy-w.png';"
  onclick="icx_openLicenseWindow(this.href); return false;"
  href="<?php print $server ?>/3.<?php print $pubid ?>?icx_id=<?php print $node->nid ?>"
  target="_blank"
  title="Main menu of all reuse options">
  <img height="25" width="27" border="0" align="left"
       alt="[Get Copyright Permissions]"
       src="<?php print $server ?>/images/icopy-w.png"/>
  Click here for reuse options!
</a>
<br />
Copyright <?php print date('Y', $node->created) ?> <?php print variable_get('site_name', '') ?><br />
<!-- iCopyright Interactive Copyright Notice -->
